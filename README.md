Bitbucket Proof
---------------

This is an OpenPGP proof that connects [my OpenPGP key](https://keyoxide.org/cf1cc5e429f055988653c17d835f28f8902ba59b) to [this Bitbucket account](https://bitbucket.org/coppie). For details check out https://keyoxide.org/guides/openpgp-proofs
